/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.java_sqlite;

import com.takitclhoke.java_sqlite.dao.UserDao;
import com.takitclhoke.java_sqlite.helper.DatabaseHelper;
import com.takitclhoke.java_sqlite.model.User;

/**
 *
 * @author ทักช์ติโชค
 */
public class TestUserDao {

    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        for (User u : userDao.getAll()) {
            System.out.println(u);
        }
//        User user1 = userDao.get(2);
//        System.out.println(user1);

//        User newUser = new User("user3", "password", 2, "F");
//        User insertedUser = userDao.save(newUser);
//        System.out.println(insertedUser);
//        insertedUser.setGender("M");
//        user1.setGender("F");
//        userDao.updata(user1);
//        User updataUser = userDao.get(user1.getId());
//        System.out.println(updataUser);

//        userDao.deleta(user1);
//        for (User u : userDao.getAll()) {
//            System.out.println(u);
//        }

        for (User u : userDao.getAllOrderBy("user_name", "asc")) {
            System.out.println(u);
        }

        DatabaseHelper.close();
    }
}
