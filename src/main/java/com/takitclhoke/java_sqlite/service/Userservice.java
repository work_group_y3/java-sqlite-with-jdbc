/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.java_sqlite.service;

import com.takitclhoke.java_sqlite.dao.UserDao;
import com.takitclhoke.java_sqlite.model.User;

/**
 *
 * @author ทักช์ติโชค
 */
public class Userservice {

    public User login(String name, String password) {
        UserDao userDao = new UserDao();
        User user = userDao.getByName(name);
        if (user != null && user.getPassword().equals(password)) {
            return user;
        }
        return null;
    }

}
