/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.java_sqlite.dao;

import com.takitclhoke.java_sqlite.model.User;
import java.util.List;

/**
 *
 * @author ทักช์ติโชค
 */
public interface Dao<T> {

    T get(int id);

    List<T> getAll();

    T save(T obj);

    T updata(T obj);

    int deleta(T obj);

    List<T> getAllOrderBy(String name, String order);

}
