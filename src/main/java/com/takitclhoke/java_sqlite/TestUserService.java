/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.java_sqlite;

import com.takitclhoke.java_sqlite.model.User;
import com.takitclhoke.java_sqlite.service.Userservice;

/**
 *
 * @author ทักช์ติโชค
 */
public class TestUserService {

    public static void main(String[] args) {
        Userservice userService = new Userservice();
        User user = userService.login("user3", "password");
        if (user != null) {
            System.out.println("Welcome user : " + user.getName());
        } else {
            System.err.println("Error");
        }
    }
}
